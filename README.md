# azure-defender

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

# you can override the SAST Job
semgrep-sast:
  variables:
    # exlude the directory
    SAST_EXCLUDED_PATHS: snippets
  tags: [ saas-linux-large-amd64 ]
  artifacts:
    reports:
      sast: gl-sast-report.json

```